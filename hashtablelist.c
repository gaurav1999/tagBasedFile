#include <glib.h>
#include <glib/gi18n.h>
#include <stdio.h>
#include <locale.h>
#include <glib-2.0/glib.h>
#include <unistd.h>
#include <limits.h>
#include<string.h>
static void destroy_elements (gpointer element)
{
  g_ptr_array_free (element, TRUE);
}
gboolean findmetadata(char dirname[]){
    struct dirent *dr;
    gchar *str="metadata";
    DIR *de=opendir(dirname);
    if(de==NULL)    {
        g_print("Directory Empty");
        return FALSE;
  }
    while((dr=readdir(de))!=NULL){
        if(!g_strcmp0(str,dr->d_name)){
            g_print("%s",dr->d_name);
            closedir(de);
            return TRUE;
        }
    }
    closedir(de);
    return FALSE;

}
GPtrArray * create_array (gchar **value, gint len){
    GPtrArray  *l=g_ptr_array_new ();
    for(gint i=0;i<len;i++){
        g_ptr_array_add (l,(gpointer)value[i]);
    }
    return l;
}
GHashTable *load_table(){
    GHashTable *table;
    GKeyFile *keyfile;
    keyfile = g_key_file_new();
    table = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, destroy_elements);
    gchar cwd[PATH_MAX];
    if (getcwd(cwd, sizeof(cwd)) != NULL)
    {
        if(findmetadata(cwd)) //if metadata present get the hash table data from the file.
        {
            gchar **keys;
            gsize length;
            char **value;
            GPtrArray *val;
            g_key_file_load_from_file (keyfile, "metadata", G_KEY_FILE_NONE, NULL);
            keys = g_key_file_get_keys (keyfile, "Tags", &length, NULL);
            for (gint i = 0; i < length; i++){
    		    gchar *key;
   		        key = keys[i];
     		    value=g_key_file_get_string_list (keyfile,"Tags",key,NULL,NULL);
     		    val=create_array(value,g_strv_length(value));
     		    g_hash_table_insert (table, key, val);
            }//end of loop

            g_key_file_free (keyfile);
 	        return table;
        }//if metadata end
	    else //metadata file not found
	    {
            return table;
	    }

    }//if getcwd end
    else{//if directory path not retrieved
        perror("Dir Path Not Recieved");
        return table;
    }
}

void insert_to_file(gpointer key, gpointer value, gpointer user_data)
{
      GKeyFile *keyfile = user_data;
      g_key_file_set_string_list (keyfile,"Tags",key,   (const char * const *)(((GPtrArray *)value)->pdata),((GPtrArray *)value)->len);
}
void print_array (gpointer value, gpointer user_data)
{
   if (value)
   {
      gchar *val = value;
      g_print (" %s\n", val);
   }
}

void print_element (gpointer key,gpointer value,gpointer user_data)
{
   if(key && value)
   {
      gchar *str = key;
      GPtrArray *valuelist = (GPtrArray *)value;
      g_print ("%s =", str);

      g_ptr_array_foreach (valuelist, print_array, NULL);
      g_print ("\n");
   }

}
void print_tags(gpointer key,gpointer value,gpointer user_data)
{
   gchar *str=key;
   g_print("%s",str);
}
void print_tag_list(GHashTable *table)
{
   g_hash_table_foreach(table,print_tags,NULL);
}
void print_menu (GHashTable *table, gboolean print_get_keys)
{
   g_print (_("Menu:\n"));
   gboolean table_size;

   table_size = g_hash_table_size (table) > 0;
   if (table_size)
   {
      g_print (_("1. View Table\n"));
   }
   g_print (_("2. Create File With Tag\n"));
   if (table_size && print_get_keys)
   {
      g_print (_("3. Get File List Under Tag\n"));
   }
   if(table_size)
    {
        g_print(_("4. Save Table to File\n"));
    }
   if(table_size)
  {
   g_print(_("5. Print All The Tags\n"));
   g_print(_("6. Delete Tag\n"));
   g_print(_("7. Delete a File\n"));
   g_print(_("8. Encrypt File\n"));
   g_print(_("9. Decrypt File\n"));
   }
   g_print("10. Exit\n");

}
unsigned long  hash(unsigned char *str)
 {
   unsigned long hash = 5381;
   int c;
   while ((c = *str++))
   {
      hash = ((hash << 5) + hash) + c;

   }
   return hash;
}
gint  encryptfile(gchar *filename , gchar *efilename, unsigned char *password)
{
	FILE *fptr1, *fptr2;
	char  c;
   unsigned long key=hash(password);
	fptr1 = fopen(filename, "r");
	if (fptr1 == NULL)
	{
		printf("Cannot open file %s \n", filename);
		exit(0);
	}
	fptr2 = fopen(efilename, "w");
	if (fptr2 == NULL)
	{
		printf("Cannot open file %s \n", efilename);
		return 0;
	}
	c = fgetc(fptr1);
	while (c != EOF)
	{
		fputc(c^key, fptr2);
		c = fgetc(fptr1);
	}
	fclose(fptr1);
	fclose(fptr2);
   return 1;

}
gint decryptfile(gchar *efilename , gchar *defilename , unsigned char *password)
{
FILE *fptr1, *fptr2;
	char  c;
   unsigned long key=hash(password);
	fptr1 = fopen(efilename, "r");
	if (fptr1 == NULL)
	{
		printf("Cannot open file %s \n", efilename);
		exit(0);
	}
	fptr2 = fopen(defilename, "w");
	if (fptr2 == NULL)
	{
		printf("Cannot open file %s \n", defilename);
		return 0;
	}
	c = fgetc(fptr1);
	while (c != EOF)
	{
		fputc(c^key, fptr2);
		c = fgetc(fptr1);
	}
	fclose(fptr1);
	fclose(fptr2);
   return 1;
}
gboolean touch(gchar *filename)
{
   FILE *fptr;
   fptr = fopen(filename, "w");
	if (fptr == NULL)
	{
		printf("Cannot create file %s \n", filename);
		return FALSE;
	}
   else
   {
      return TRUE;
   }
   fclose(fptr);

}
gboolean removefile(gchar *filename)
{

   if(!remove(filename))
   {
      return TRUE;
   }
   else
   {
      return FALSE;
   }

}
void touchfile(GHashTable *table)
{
   gchar key[50] , value[50];
   g_print (_("Enter Tag\n"));
   scanf ("%s", key);
   printf (_("Enter Filename\n"));
   scanf ("%s",value);
   if(touch(value))
   {
      if (g_hash_table_lookup (table, (gpointer)key))
         {
            GPtrArray *valuelist = g_hash_table_lookup(table, key);
            g_ptr_array_add (valuelist, (gpointer)g_strdup(value));
         }
         else
         {
            GPtrArray *valuelist = g_ptr_array_new_with_free_func (g_free);
            g_ptr_array_add (valuelist, (gpointer)g_strdup (value));
            g_hash_table_insert (table, (gpointer)g_strdup (key), (gpointer)valuelist);
         }

   }
   else
   {
      g_print(_("File not created\n"));
   }

}
void removefilefromtable(GHashTable *table , gchar *tag , gchar *filename)
{
   guint index;

   if(removefile(filename))
   {
      if(g_hash_table_contains(table,tag))
      {
         GPtrArray *valuelist = g_hash_table_lookup(table, tag);
         if(g_ptr_array_find_with_equal_func(valuelist,(gpointer)filename,g_str_equal,&index))
         {

            g_ptr_array_remove_index(valuelist,index);

         }

      }
      g_print(_("File removed successfully\n"));

   }
   else
   {
      g_print(_("Can not remove file \n"));
   }


}
void deleteTag(GHashTable *table)
{
   gchar key[50];
   g_print (_("Enter Key\n"));
   scanf("%s", key);
   if (g_hash_table_contains(table, key))
   {
      GPtrArray *valuelist = g_hash_table_lookup(table, key);
      g_print ("%s =", key);
      g_ptr_array_foreach (valuelist, print_array, NULL);
      g_print ("\n");
   }

   else
   {
      g_print (_("key %s not found\n"), key);
   }
}
gboolean checkpassword(unsigned char *password)
{

   gboolean num=FALSE;
   gboolean upper=FALSE;
   gboolean sp=FALSE;
   for(int i=0;i<strlen((const char*)password);i++)
   {
      printf("%c\n",password[i]);
      if(password[i]>=48 && password[i]<=57 )
      {
         num=TRUE;
         printf("Number Detetcted");
      }
      if(password[i]>=65 && password[i]<=90)
      {
         upper=TRUE;
         printf("Upper Character Detected");
      }
      if((password[i]>=32 && password[i]<=47) ||(password[i]>=58 && password[i]<=64)||(password[i]>=91 && password[i]<=95) )
      {
         sp=TRUE;
         printf("Special Char Detected");
      }


   }
   if(num==TRUE && upper==TRUE && sp==TRUE)
   {
      return TRUE;
   }
   return FALSE;
}
void encryptfilemenu(GHashTable *table)
{
   gchar filename[50],value[50];
   unsigned char password[20];
   g_print(_("Enter File name which you want to encrypt\n"));
   scanf ("%s",filename);
   g_print(_("Enter File name to save enrcypted file with\n"));
   scanf("%s",value);
   g_print(_("Enter Password \n"));
   scanf("%s",password);
   gboolean strong=FALSE;
   strong=checkpassword(password);
   while(strlen((const char*)password)<10 && strong==FALSE)
   {
      g_print("Password is not strong please enter with min 10 length , and 1 min(Special char , Upper Case char , Digit)");
      scanf("%s",password);
      strong=checkpassword(password);

   }
   if(encryptfile(filename,value,password))
   {
      if (g_hash_table_lookup (table, (gpointer)"encrypted"))
      {
         GPtrArray *valuelist = g_hash_table_lookup(table,"encrypted");
         g_ptr_array_add (valuelist, (gpointer)g_strdup(value));
      }
      else
      {
         GPtrArray *valuelist = g_ptr_array_new_with_free_func (g_free);
         g_ptr_array_add (valuelist, (gpointer)g_strdup (value));
         g_hash_table_insert (table, (gpointer)g_strdup ("encrypted"), (gpointer)valuelist);
      }
      g_print(_("File Encrypted\n"));

   }
   else
   {
      g_print(_("File can't be encrypted\n"));
   }

}
void savetabletofile(GKeyFile *keyfile ,GHashTable *table)
{
   g_hash_table_foreach (table,insert_to_file, keyfile);
   g_key_file_save_to_file (keyfile, "metadata", NULL);
}
gint main (gint argc, gchar **argv)
{
   int choice;
   GHashTable *table;
   guint index;

   table=load_table();

   GKeyFile *keyfile;
   keyfile = g_key_file_new();

   gchar key[50] , value[50];
   gchar filename[50];
   unsigned char password[10];



   g_print (_("Welcome to Hash Table Function\n"));
   print_menu (table, FALSE);
   while (scanf ("%d", &choice))
   {
      switch(choice)
      {
         case 1:
            g_print (_("Printing values:\n"));
            g_hash_table_foreach (table, print_element, NULL);
            break;

         case 2:
            touchfile(table);
            break;

         case 3:
            deleteTag(table);
            break;

         case 4:
            savetabletofile(keyfile,table);
            break;

         case 5:
            print_tag_list(table);
            break;

         case 6:
            g_print(_("Enter Tag to delete\n"));
            scanf("%s",key);
            if(g_hash_table_contains(table,key))
            {
               g_hash_table_remove(table,key);
            }
            else
            {
               g_print(_("Key Not Found Can not delete\n"));
            }
            break;

         case 7:
            g_print(_("Enter Tag \n"));
            scanf("%s",key);
            g_print(_("Enter Filename to delete\n"));
            scanf("%s",value);
            removefilefromtable(table, key , value);

            break;

         case 8:
            encryptfilemenu(table);
            break;

         case 9:
            g_print(_("Enter File name which you want to decrypt\n"));
            scanf ("%s",filename);
            g_print(_("Enter File name to save decrypted file with\n"));
            scanf("%s",value);
            g_print(_("Enter Password \n"));
            scanf("%s",password);
            if(decryptfile(filename,value,password))
            {
               if(g_hash_table_contains(table,"encrypted"))
               {
                  GPtrArray *valuelist = g_hash_table_lookup(table,"encrypted");
                  if(g_ptr_array_find_with_equal_func(valuelist,(gpointer)filename,g_str_equal,&index))
                  {
                     g_print("%d =\n",index);
                     g_ptr_array_remove_index(valuelist,index);

                  }

               }

            }
            break;

         case 10:
            savetabletofile(keyfile,table);
            g_print(_("Work Saved Exit Succesfull\n"));
            return 0;

         default:
            g_print (_("Unknow option\n"));
            break;
      }
      print_menu (table, TRUE);
   }
   g_hash_table_remove_all (table);
   g_hash_table_unref (table);
   g_key_file_free (keyfile);
   return 0;
}



