## Intro
A files-tagging app built using GLib-core

## Compiling
- meson build
- ninja -C build

## Running
./build/hashtablelist


## Description (modules)

- hashtable module {final module every module will be integrated finally here}
- filehash module {used to read metadata file into hash-table}
- GKey-file load module {used to fetch hash-table contents from the metadata-file}

WIP: A lot to change and document , please bear with the formatting and please do raise MR if you wish to contribute.
